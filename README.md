# FUMACINHA ROBOT

## Descrição

 Usando como inspiração o robô Corner, o fumacinha utiliza os princípios dos robôs corners e wall. Inicialmente o robô torna seu eixo em um ângulo reto e se move até a parede, se reposiciona à 90 graus e direciona seu canhão ao centro da arena.
  
  Daí, em nossa primeira tarefa da nossa rotina while, verificamos se nosso tank está à 50 pixels de alguma das paredes, se ocorre, então o tank é levado 100 pixels para trás e realzia uma manobra de 90 graus desviando da parede, em segunda instrução é colocada o método para o robô andar um distância grande, em paralelo com as demais, garantindo que ele estará sempre em movimento. Os comandos seguintes de mudança de ângulo do eixo, alternando de quadrantes, é usado para o movimento ser feito em zigue-zague, tornando o nosso robô um alvo difícil de acertar, porquê mesmo que o adiversário tente calcular nossa distância e tentando prever nossa próxima localização, usando um pouco de pitágoras e sabendo nossa velocidade média, o inimigo falhará, pois não nos movemos apenas reto, e isso altera nossa distancia, consequentemente saindo da rota de tiro.
  
  Em nosso sistema de disparo é levado em consideração a distancia do alvo, se está a menos de 200 pixels, o que significa que será mais fácil de acertá-lo, paramos nossa rotina, o tank fica imóvel, apenas realizando disparos e a cada disparo e realizado o radar verifica se o alvo está no alcance, para alvos acima de 200 pixels utiliza-se menos poder de fogo, para que não tenha muito consumo energético e o tempo de resfriamento do canhão não seja longo, poupando para eventuais confrontos de tanks mais próximos a nós.

  **Created by Emanuel Vieira.**