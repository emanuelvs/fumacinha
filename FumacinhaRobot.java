package FEMAN;

import robocode.*;

import java.awt.*;

public class FumacinhaRobot extends AdvancedRobot{
    double maxMovement;
    double areaX;
    double areaY;
    public void run() {

        setColors(Color.black,Color.red,Color.blue);
        areaX = getBattleFieldWidth();
        areaY = getBattleFieldHeight();
        maxMovement = Math.max(areaX, areaY);
        turnLeft(getHeading() % 90);
        ahead(maxMovement);
        turnRight(90);
        turnGunRight(90);

        while(true) {
            checkBorders();
            setAhead(10000);
            turnRight(30);
            waitFor(new TurnCompleteCondition(this));
            turnLeft(60);
            waitFor(new TurnCompleteCondition(this));
            turnRight(30);
            waitFor(new TurnCompleteCondition(this));
            checkBorders();
            turnGunRight(360);
        }
    }

    public void checkBorders() {
        double left = getX();
        double bottom = getY();
        double right = areaX - left;
        double top = areaY - bottom;
        if(Math.min(left, right) <= 50 || Math.min(top, bottom) <= 50){
            back(100);
            turnRight(90);
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        freeFire(e.getDistance());
    }
    public void freeFire(double targetDistance) {
        if(targetDistance < 200) {
            stop();
            fire(3);
            scan();
            resume();
        }
        else {
            fire(1);
        }


    }

    public void onHitRobot(HitRobotEvent e) {
    }
}
